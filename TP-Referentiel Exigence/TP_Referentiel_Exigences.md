# TP Création du Référentiel de tests

L’objectif du TP est de comprendre la méthodologie de définition des besoins pour créer et tester une application. Le Tp permet de créer un référentiel d’exigences puis les cas de tests associés.
Le livrable de ce TP est un dossier de tests présentant toutes les exigences, les cas de tests et la maquette de l’application au fil de fer.

## Cahier des charges

Le client demande une application permettant de gérer les informations d’authentification aux différentes ressources web. Référencées sous forme de clés, l’utilisateur renseigne au sein d’un portefeuille l’url d’accès, l’identifiant et le mot de passe. Pour faciliter le renseignement de données complémentaires, l’utilisateur pourra également renseigner une information de commentaire.

Sur le plan technique, l’application sera disponible en local sur le poste de l’utilisateur et devra fournir des Api pour accéder ou modifier l’infirmation. Une interface Web permet également la saisie des informations, les utilisateurs accèdent à cette interface depuis le navigateur Chrome ou Firefox. Vu le type d’information, il est évident de prévoir un procédé permettant de stocker l’information dans un système de persistance cryptée.

L’application devra fournir à l’utilisateur un tableau des informations triées de telle sorte que la dernière modification apportée se retrouve en haut du tableau.

Seule la clé (url) est une information obligatoire, en effet l’utilisateur peut préparer sa clé sans information.
En revanche lorsqu’un identifiant est saisi, le mot de passe est obligatoire. Ce dernier doit avoir une majuscule, une minuscule et un chiffre, et une longueur minimum de 6 caractères

L’utilisateur peut rechercher ses informations soit depuis l’url, soit depuis l’identifiant.

Pour garantir l’intégrité de l’information, l’utilisateur ne peut avoir 2 clés identiques, en revanche il doit pouvoir renseigner 2 fois la même url.

A l’ajout, ou modification d’une url, le système doit pouvoir vérifier que l’information saisie est bien une url.

L’utilisateur peut supprimer une clé, mais dans ce cas, il devra s’authentifier sur l’application.
Il est à noter que l’application doit être performante.

L’utilisateur peut modifier une information seulement après sélectionné une clé.


## Atelier : 

Sur la base de ce cahier des charges ci-dessus et des questions posées en séance que vous orienterez à partir d’exemple de données pour élucider et affiner ces exigences, vous créer un référentiel d’exigences et de test ainsi qu’une maquette fil de fer permettant de dessin l’interface de votre application. 




## Méthodologie :

Chaque fonctionnalité de l’application devra faire l’objet d’une exigence qui aura ses cas de tests :

L’exigence devra présenter les éléments suivant :
*	Code exigence (tag unique qui permettra de faire référence dans tout type de document associé au projet)
*	Exigence (présentation de ce que doit faire la fonctionnalité). L’exigence est présentée sous forme de :
    *	userstory (En tant que… je souhaite… afin de ) 
    *	Gerkin  Etant donné que …Quand... Alors 
*	Criticité (niveau de risque si cette fonctionnalité venait à manquer - important/Moyen/Faible)
*	Catégorie (norme iso9126 : extrait suivant : Aptitude fonctionnelle / performance/
interopérabilité / sécurité)
*	Type d’exigence : Fonctionnelle/Non fonctionnelle

Chaque exigence porte ses cas de tests en présentant les éléments suivants :
-	N cas de tests (tag unique qui permettra de faire référence dans tout type de document associé au projet)
-	Cas de test : libellé court qui explique pourquoi ce cas est différent des autres
-	Action/Donnée (Séquence permettant de présenter l’enchainement des interactions et le jeu de donnée associé)
-	Point de contrôle (résultat attendu, quel est l’élément à contrôler permettant de justifier le résultat de tests)








